/*
 * dbg_pin.h
 *
 *      Author: dschnell
 */

#ifndef DBG_PIN_H_
#define DBG_PIN_H_

#include <stdint.h>
#include <stdbool.h>

/* Function declarations */

void dbg_pin_init();
void dbg_pin(uint8_t pin_mask, bool on);

#endif /* DBG_PIN_H_ */

/* EOF */

