/**
 *****************************************************************************
 * @title   SPI.h
 * @author  Rainer Becker
 * @date    02.Apr. 2013
 * @brief   This functions controls the SPI interfaces (iMX6 as slave and the
 *          rest as master)
 *******************************************************************************/

#ifndef __SPI_H__
#define __SPI_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <sys/types.h>
#include <stdbool.h>
#include "stm32f0xx_conf.h"
#include "stm32f0xx.h"

void init_spi1();
void init_spi2();

enum spi_port { SPI_NO_VALID_PORT = 0, SPI_HOST2DIAG_COM_PORT = 1, SPI_DIAG_MEM_PORT = 2};

void* spi_open(int instance);
ssize_t spi_write(void* spi_inst, const char* buf, size_t cnt, int ticks);
ssize_t spi_write_partial(void* spi_inst, const char* buf, size_t cnt,
        int ticks);
ssize_t spi_transmit(void* spi_inst, const char* tx_buf, size_t tx_len,
		char* rx_buf, size_t rx_len, int ticks);
ssize_t spi_read(void* instance, char* buf, size_t cnt, int ticks);
void spi_set_rx_transfer_buffer(void* instance, char* buf, size_t cnt);
SPI_TypeDef* spi_get_spi_ptr(void* spi);
bool spi_is_cs_active(void* spi_inst);
void spi_disable(void* spi);
void spi_enable(void* spi);

#ifdef __cplusplus
}
#endif

#endif /* __SPI_H__ */

/* EOF */
