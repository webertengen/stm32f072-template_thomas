/**
 * @file
 *
 *****************************************************************************
 * @title   spi_irq.h
 * @author  Daniel Schnell (deexschs)
 *
 * @brief   SPI irq functions
 *
 *******************************************************************************
 */

#ifndef __SPI_IRQ_H__
#define __SPI_IRQ_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "spi_priv.h"

void spi_irq_init(DiagSPI* spi);
void spi_tx_isr_enable(SPI_TypeDef* SPIx);
void spi_tx_isr_disable(SPI_TypeDef* SPIx);
void spi_rx_isr_enable(SPI_TypeDef* SPIx);
void spi_rx_isr_disable(SPI_TypeDef* SPIx);
bool spi_isr_tx_irq_finished(DiagSPI* spi, signed portBASE_TYPE* xHigherPriorityTaskWoken);

#ifdef __cplusplus
}
#endif

#endif /* __SPI_IRQ_H__ */

/* EOF */
