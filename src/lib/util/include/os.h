/**
 * @file
 *****************************************************************************
 * @title   os.h
 * @author  Daniel Schnell (deexschs)
 *
 * @brief   OS additions for functionality not available in freertos.
 *******************************************************************************/

#ifndef _OS__H
#define _OS__H

#ifdef _cplusplus
extern "C"
#endif

/* Includes ------------------------------------------------------------------*/
#include <task.h>

/* Function declarations -----------------------------------------------------*/

/**
 * Adds Task Control Block to list of the monitor command for the purpose of debugging.
 *
 * @param   tcb     Task Control Block.
 *
 * @note this function is not reentrant.
 */

void            os_add_tcb(xTaskHandle tcb);

/**
 * Get next task control block in os tcb list. This function is used for iterating over
 * list of created tasks. If Null is given as argument, first element in list is returned.
 * In providing the returned pointer to the next call to  os_tcb_next() the next tcb is returned.
 * If no more elements are available in the list, NULL is returned.
 *
 * @param tcb       NULL or pointer of previously returned tcb value
 *
 * @return          next tcb insode os tcb �list after given element.
 *
 * @note this function is not reentrant.
 */
xTaskHandle    os_tcb_next(xTaskHandle tcb);

#ifdef _cplusplus
}
#endif

#endif  /* _OS__H */

/* EOF */

